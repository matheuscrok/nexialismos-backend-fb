package com.projectnexialismosbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectnexialismosbackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectnexialismosbackendApplication.class, args);
    }

}
