package com.projectnexialismosbackend.rest;



import java.util.List;

import com.facebook.ads.sdk.APIContext;
import com.facebook.ads.sdk.APIException;
import com.facebook.ads.sdk.APINodeList;
import com.facebook.ads.sdk.AdAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectnexialismosbackend.dao.CampaignDao;
import com.projectnexialismosbackend.entity.Campaign;

@RestController
@RequestMapping("/campaign")
public class CampaignRest {

    @Autowired
    private CampaignDao campaignDao;


    @GetMapping("/get")
    public List<com.facebook.ads.sdk.Campaign> get() throws APIException {
                String access_token = "EAAg2aCFs9x4BANfZArT9Xj1CGeAh4gG7xo7sMIIIGmTISLuYtj3IZADrvnaM8WnXGmVPYJ9rzqMfBOsains3r9b5ae29ZAgsZCyVqAZAiiOZBS4dLJXP1TVHiwAHSSo65OQHK4NotZAwP93JYWCaNtMbxG1MU1lKL8kQVpqmY3iIaiuP3SOmwQZBJSGR87lirVcuDAAli2FydKaIXzZAY2ek9ABNL6gcjWZAZCM2LWgPlK5GFToZAH6fPdcawLDE7jW2DfEZD";
        String app_secret = "c2e094fecd8b7d78a5d512360b2893e5";
        String app_id = "243699854229117";
        String id = "act_545284399911504";
        APIContext context = new APIContext(access_token).enableDebug(true);

        APINodeList<com.facebook.ads.sdk.Campaign> listCampaign=new AdAccount(id, context).getCampaigns()
                .requestAllFields()
                .execute();

        return listCampaign;

    }


}
