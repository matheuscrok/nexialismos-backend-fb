package com.projectnexialismosbackend.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projectnexialismosbackend.entity.Campaign;

@Repository
public interface CampaignDao extends JpaRepository<Campaign, Integer>{




}
